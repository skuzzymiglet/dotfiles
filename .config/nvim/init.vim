call plug#begin('~/.vim/plugged')

Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'morhetz/gruvbox'
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dense-analysis/ale'
Plug 'mattn/emmet-vim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'tpope/vim-commentary'
Plug 'baskerville/vim-sxhkdrc'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'preservim/nerdtree'
Plug 'ActivityWatch/aw-watcher-vim'
Plug 'tckmn/vim-xsami'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'miyakogi/seiya.vim'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-surround'
Plug 'mhinz/vim-startify'
Plug 'https://github.com/jreybert/vimagit'

call plug#end()

filetype plugin on

set laststatus=2
set encoding=utf-8
set fileencoding=utf-8
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set number relativenumber

let g:gruvbox_italic=1
colorscheme gruvbox
let g:airline_theme='gruvbox'
set bg=dark

filetype indent on

let g:mkdp_auto_start=0
let g:mkdp_browser="qutebrowser"
let g:mkdp_page_title = "${name}"
let g:vim_markdown_folding_disabled=1

let g:seiya_auto_enable=1

let g:ale_linters = {'javascript': ['eslint'], 'python': ['flake8']}

let g:user_emmet_leader_key=','
let g:UltiSnipsExpandTrigger="<c-u>"
let g:UltiSnipsJumpForwardTrigger="<c-l>"
let g:UltiSnipsJumpBackwardTrigger="<c-h>"

let g:user_emmet_install_global = 0
autocmd FileType html,css,js EmmetInstall

cnoreabbrev tlo TlistOpen
cnoreabbrev gr GoRun
cnoreabbrev gd GoDoc
imap <leader><leader>i <esc>:call ToggleIPA()<CR>a
nnoremap <leader>. :CtrlPTag<cr>
map <c-n> :NERDTreeToggle<cr>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow splitright

nnoremap <C-F> :FZF -e<cr>

autocmd BufWritePost *.ly !lilypond %; pidof zathura || zathura $(sed 's/\.ly/\.pdf/g' <(echo %))&
nnoremap <C-p> :!playly %& &> /dev/null<cr>
