#	"sort" If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
#ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
#export LANG="en_GB.UTF-8"
#export LC_ALL="en_GB.UTF-8"
export LANG=en_GB.UTF-8
export LC_CTYPE="en_GB.UTF-8"
export LC_NUMERIC="en_GB.UTF-8"
export LC_TIME="en_GB.UTF-8"
export LC_COLLATE="en_GB.UTF-8"
export LC_MONETARY="en_GB.UTF-8"
export LC_MESSAGES="en_GB.UTF-8"
export LC_ALL=en_GB.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compaudit compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# End of lines added by compinstall

# Path

export PATH="$HOME/go/bin:$HOME/.local/bin:$HOME/.gem/ruby/2.6.0/bin:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin/:/usr/lib/jvm/java-11-openjdk/bin/:node_modules/.bin/:$HOME/flutter/bin/:$HOME/bin/:$PATH"
export PATH="${PATH}:/opt/android-sdk/tools/bin/"
export STRING=""
export BROWSER="qutebrowser"
export EDITOR="nvim"
export FILE="lf"
export TERMINAL="st"
export GOPATH="$HOME/go"
export XDG_CONFIG_HOME
export ANDROID_HOME="/opt/android-sdk/"
export PATH="$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH"
export _JAVA_AWT_WM_NONREPARENTING=1

#vifmrc

export MYVIFMRC="~/.vifmrc"

# Correction

#ENABLE_CORRECTION="true"

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}

zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() {
        echo -ne '\e[5 q'
} # Use beam shape cursor for each new prompt.

# 1 letter aliases

alias s="sudo"
alias v="nvim"
alias p="pacman"
alias y="yay"
alias c="clear"
alias x="exit"
alias m="neomutt"
alias se="googler"
alias cx="chmod +x"
alias sd="shutdown 0"

# sane defaults

alias ls="ls --color=always"

# wifi

alias uwf="sudo ip link set wlp1s0 up"
alias dwf="sudo ip link set wlp1s0 down"
alias swm="sudo wifi-menu"
alias pgc="ping google.com"
alias iplk="ip link"

# make vifm alias to use current dir

alias vfm="vifm ."

# feh always scale

alias feh="feh --scale"

# pacman

alias spsyu="sudo pacman -Syu"
alias sprsc="sudo pacman -Rsc"
alias pqi="pacman -Qi"

# yay 

alias ysyu="yay -Syu"
alias yrsc="yay -Rsc"
alias yqi="yay -Qi"

# both

alias ys="yay -Syu --noconfirm"

# pip always user

alias pipi="pip install --user"

# configs

alias vi3c="nvim ~/.config/i3/config"
alias vzrc="nvim ~/.zshrc"
alias vvrc="nvim ~/.vimrc"
alias viv="nvim ~/.config/nvim/init.vim"
alias vvfrc="nvim ~/.config/vifm/vifmrc"
alias svsmb="sudo nvim /etc/samba/smb.conf"
alias svnmrc="sudo nvim /etc/neomuttrc"
alias vdrc="nvim ~/.config/dunst/dunstrc"
alias vqbc="nvim ~/.config/qutebrowser/config.py"
alias vxr="nvim ~/.Xresources"
alias vud="nvim ~/.config/udiskie/config.yml"
alias vnb="nvim ~/.newsboat/urls"
alias vbs="nvim ~/.config/bspwmrc"
alias vsx="nvim ~/.config/sxhkdrc"
alias vpbc="nvim ~/.config/polybar/config"
alias vlf="nvim ~/.config/lf/lfrc"
alias vz="nvim ~/.config/zathura/zathurarc"

# mounts

alias um="udisksctl mount -b"

alias mchh="sudo mount -t cifs //192.168.1.234/home /mnt/chunkyhome/ -o iocharset=utf8"
alias mchrc="sudo mount -t cifs '//192.168.1.234/Ripped CDs' /mnt/ripped_cds/ -o iocharset=utf8"
alias mshr="udisksctl mount -b /dev/sda6"
alias mstr1="udisksctl mount -b /dev/sda5"

alias umchh="sudo umount //192.168.1.234/home"
alias umchrc="sudo umount '//192.168.1.234/Ripped CDs'"
alias umshr="udisksctl unmount -b /dev/sda6"
alias umstr1="udisksctl unmount -b /dev/sda5"

# menus

alias cdm="ls /mnt/Shared/cd/**/*.ogg /mnt/Shared/cd/**/*.wav | rofi -dmenu | xargs chronic ffplay -loop 0  &"
alias rcd="ls /mnt/Shared/cd/**/*.ogg /mnt/Shared/cd/**/*.wav | shuf -n 1 | sed -e 's/ /\\ /g' | xargs chronic ffplay -loop 0 &"

# git

alias ga="git add"
alias gc="git commit"
alias gaa="git add all"
alias gd="git diff"
alias gcam="git commit -am"
alias gpuom="git push -u origin master"
alias gpuoa="git push -u origin --all"
alias gsp="git status --porcelain"
alias adog="git log --all --decorate --oneline --graph"

# git repo for dotfiles
alias dcnf='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# grub

alias sgmc="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# applications

alias br=$BROWSER

# pomodoro

alias pmd="pomo s -p"

# weather
alias cwi="curl wttr.in"

# termdown

alias td="termdown"

# gpodder

alias gud="gpo update && gpo download"

alias nb="newsboat"
alias py="python3"

# mailbox

alias ms="mailsync"

# discourage cat usage

alias cat="echo CAT BAD! && cat"

# youtube-dl

alias ydl="youtube-dl -f 'bestvideo[height<=1080]+bestaudio/best[height<1080]'"

# hugo

alias hgb="rm -rf public/; hugo"
alias hgn="hugo new --editor nvim"

autoload zkbd
#[[ ! -f ${ZDOTDIR:-$HOME}/.zkbd/$TERM-$VENDOR-$OSTYPE ]] && zkbd
source $HOME/.zkbd/st-256color-:0

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char
#bindkey "${key[£]}" £

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null

# powerline-go

function powerline_precmd() {
     eval "$(powerline-go -max-width 50 -error $? -shell zsh -eval -modules-right git)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi

if [[ ! -e .zsh/zsh-autosuggestions/ ]] then;
    git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
fi

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_USE_ASYNC=1
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
