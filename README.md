## My Arch Dotfiles

Most of my setup. Read my [blog article](https://skuzzymiglet.github.io/posts/setup/) on it 

I use:

+ Arch Linux
+ bspwm
+ sxhkd for keybindings
+ st (Luke Smith's fork but hoping to add stuff to it)
+ vim
+ qutebrowser
+ mpv
+ lf
+ polybar

Not on the repo:

+ Weechat for Discord and Matrix - will add soon
+ ytop for system monitoring
+ WIP fork of `slock` to blur the screen with `MagickWand`
+ [Scripts](https://github.com/skuzzymiglet/bin) that I'll merge into `.local/bin/` in this repo soon

## Installation

WIP - need to make `progs.csv` for LARBS

![Browser, vim, gomuks, ytop, vifm, neomutt](screenshot1.png)
![neofetch, Arch Linux, configs, vim](screenshot2.png)
